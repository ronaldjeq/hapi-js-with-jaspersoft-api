
const dbOpts = {
    url: process.env.MONGO_DB_CONN,
    settings: {
        poolSize: 10,
        // auth:{
        //   user: process.env.MONGO_DB_USER,
        //   password: process.env.MONGO_DB_PWD
        // },
        useNewUrlParser: true
    },
    decorate: true,
  };
  
  module.exports = dbOpts;