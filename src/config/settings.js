
'use strict';
require('dotenv').config();
const settings = {
    baseUrl: `http://${process.env.ServerWithJasperIO}:${process.env.portServer}/jrio/rest_v2/reportExecutions`,
};

module.exports =settings;