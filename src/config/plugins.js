'use strict';
var internals = {};
const HapiSwagger = require('hapi-swagger'),
    Inert = require('inert'),
    Vision = require('vision'),
    Hoek = require('hoek'),
    Pack = require('../../package');
    const dbOpts = require('./mongodb');

const swaggerOptions = {
    info: {
        'title': 'JasperSoft with Hapi - API Documentation',
    },

    grouping: 'tags'
};


let plugins = [];

plugins = plugins.concat([
    Inert,
    Vision,
    {
        plugin: HapiSwagger,
        options: {
            info: {
                'title': 'SENATI - API Documentation',
            },
            grouping: 'tags'

        }
    }
]);
plugins.push({
    plugin: require('hapi-mongodb'),
    options: dbOpts
  })

console.log("Connection to mongo url", dbOpts.url );

plugins.push(
    {
        plugin: require('good'),
        options: {
            ops: {
                interval: 1000
            },
            reporters: {
                consoleReporter: [{
                    module: 'good-squeeze',
                    name: 'Squeeze',
                    args: [{
                        log: '*',
                        response: '*'
                    }]
                }, {
                    module: 'good-console'
                }, 'stdout'],
                fileReporter: [{
                    module: 'good-squeeze',
                    name: 'Squeeze',
                    args: [{
                        'response': '*',
                        'request-error': '*'
                    }]
                }, {
                    module: 'good-formatters',
                    name: 'Apache',
                    // pre-defined formats are 'combined', 'common', 'referrer'
                    args: [{
                        format: 'common'
                    }]
                }, {
                    module: 'good-file',
                    args: ['./app-server.log']
                }]
            }
        }
    },

);
module.exports = plugins;