"use strict";
const settings = require('./settings');
const axios = require('axios') ;
const Mailchecker = require("mailchecker");
const MailGun = require("mailgun-js");
const 
    //Hapi itself
    Hapi = require('hapi'),
    // kind of like underscore, but specific to Hapi
    Hoek = require('hoek'),
    // some additional services
    Plugins = require('./plugins'),
    Boom = require('boom'),
    // connection with mongodb
    mongodb= require('hapi-mongodb'),
    // the routes we'll support
    Routes = require('./routes');

    let server = Hapi.server({
        port: process.env.NODE_PORT, // 3000
        address: process.env.NODE_IP, //0.0.0.0
    });
    let callApi= true;
    exports.init = async() =>{
        await server.register(Plugins).catch(e=>{console.log(e)});
        await Routes.init(server);
        await server.initialize();
        return server;
    }
    const sendAlert = async(urlReport,user) => {
        console.log('iniciando envío de alerta')
        const htmlTemplate = require("./template");

        const mail_api = new MailGun({
            apiKey: process.env.MAIL_GUN_TOKEN,
            domain: process.env.MAIL_GUN_DOMAIN
        }); 
        const options = {
        from: process.env.MAIL_GUN_FROM || "app@senati.edu.pe",
        to: user,
        subject: "SENATI: Tu código de verificación",
        html: htmlTemplate.replace("%CODE_EMAIL%", `${urlReport}`)
    };
        mail_api.messages().send(options, (error, body) => {
        if (error || body === undefined) {
            console.log("======= Error: send_message =======");
            console.log(error);
            console.log("===============================");
            return false
        }
        else{
            return true
        }
        })
    }
    const callApis = async() => {
        //alert('holi');
        const db = server.mongo.db;
        const myCollection =db.collection('jasper_report_description');
        const query = { show_report: false };
        const {baseUrl}=settings;
        let isFinishedReport=false;
        myCollection.find(query).toArray(function(err, result) {
            result.map(item=>{
                  axios({
                    method:'get',
                    url:`${baseUrl}/${item.id_execute_report}/status`,
                }).then(response=> {
                    if(response.data.value==='ready'){
                        isFinishedReport=true;
                        return axios({
                            method:'post',
                            url:`${baseUrl}/${item.id_execute_report}/exports`,
                            data:{
                              outputFormat: item.outputFormat,
                              attachmentsPrefix: "./images/",
                          }
                            
                        })
                    }
                    //console.log(response);
                  }).then(response=>{
                      //console.log(response)
                   if (isFinishedReport){
                        try{   
                          let urlReport ="";
                          let updatedData;
                          urlReport= `${baseUrl}/${item.id_execute_report}/exports/${response.data.id}/outputResource`

                          if(item.email!==null){
                             updatedData = sendAlert(urlReport,item.email ) 
                          }
                          else{
                             updatedData = true;
                          }
                             if(updatedData){
                                const myquery={id_execute_report:item.id_execute_report}
                                const newValuesForList={ $set: {show_report:true,link_report:urlReport}}  
                                const ListReportCollection =db.collection('jasper_report_description');
                                ListReportCollection.updateOne(myquery, newValuesForList, function(err, res) {
                                    if (err) throw err;
                                    })
                                }

                            }   
         
                        catch(e){
                            console.log(e)
                        }
                    }
                  })
                       
                     // console.log('asdsadasdsa')
                  .catch(error => {
                    console.log("-------");
                    console.log("Hubo un error al buscar el status del reporte", (error.resposne));
                    console.log("-------");
                  });
           })         
    });

        console.log('aquí va la función para consultas periódicas', new Date())
        setTimeout( ()=> { 
            callApis() 
        } ,  5000 )
    }


    const startServer = async() => {
        try {
            await server.start();
            console.log(`server started at: ${server.info.uri} with env: ${process.env.NODE_ENV}`);
            const gracefulStopServer = async () => {
                server.stop({ timeout: 10 * 1000 }, () => {
                    process.exit(0)
                });
            }
            await callApis();
            process.on('SIGINT', gracefulStopServer)
            process.on('SIGTERM', gracefulStopServer);




        } catch (error) {
            Hoek.assert(!error, error);
            console.log('====================================');
            console.log(error);
            console.log('====================================');
            process.exit(1)
        }
    };

    exports.deployment = async () => {
        try {
            await server.register(Plugins);
            await Routes.init(server);
    
        } catch (error) {
            Hoek.assert(!error, error);
            throw error
        }
        // configure jwt strategy
    
        // // set routes
        await startServer();
    
        return server;
    };
    

    