/**
 * # account/endpoints.js
 *
 * To login and get main user information
 *
 */
'use strict';
/**
 * ## Imports
 *
 */
//Handle the endpoints
const ClientHandlers = require('./handlers');
const Joi = require("joi");

let internals = {};
/**
 * ## Set the method, path, and handler
 *
 * Note the validation of the account/register parameters
 *
 */
internals.endpoints = [
  {
    method: 'POST',
    path: '/searchReport/',
    config: {
      timeout: {server: false},
      handler: ClientHandlers.createReportID,
      cors: {
        origin: ['*'],
        additionalHeaders: ['cache-control', 'x-requested-with']
      },
      // Include this API in swagger documentation
      description: 'Búsqueda de cliente',
      notes: 'Pasando por las opciones apropiadas, puede buscar el cliente disponible en el sistema',
      tags: ['api', 'client'],
      plugins: {
        "hapi-swagger": {
          responses: {
            "200": {
              description: "Resultado de búsqueda que coincide con la cadena enviada",
       },
            "400": {
              description: "entrada de parámetro erronea",
            },
          }
        }
      },

      validate: {
        payload: {
          queryParameters: Joi.array().items(Joi.object({
            name:Joi.string().example("IndicatorMail"),
            value:Joi.array().example(["Y"])
          }))
          .required(),
          pathField: Joi.string()
          .required()
          .example('/samples/reports/test3'),
          userEmail: Joi.string()
          .example('ronaldjeq@gmail.com'),
          userName: Joi.string()
          .required()
          .example('111012'),
          description: Joi.string()
          .required()
          .example('Reporte de títulos para el id 111012'),
          outputFormat: Joi.string()
          .required()
          .example('pdf'),
        }
      }
    }
  
  },

  {
    method: 'GET',
    path: '/searchReport/{user}/list',
    config: {
      timeout: {server: false},
      handler: ClientHandlers.listReports,
      cors: {
        origin: ['*'],
        additionalHeaders: ['cache-control', 'x-requested-with']
      },
      // Include this API in swagger documentation
      description: 'Búsqueda de cliente',
      notes: 'Pasando por las opciones apropiadas, puede buscar el cliente disponible en el sistema',
      tags: ['api', 'client'],
      plugins: {
        "hapi-swagger": {
          responses: {
            "200": {
              description: "Resultado de búsqueda que coincide con la cadena enviada",
       },
            "400": {
              description: "entrada de parámetro erronea",
            },
          }
        }
      },

      validate: {
        params: {

          user: Joi.string()
          .required()
          .example('111012'),
        }
      }
    }
  
  }
];

module.exports = internals;
