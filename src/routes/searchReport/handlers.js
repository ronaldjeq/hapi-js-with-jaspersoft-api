/**
 * # restricted/handlers.js
 *
 * Display simple message if user has access
 *
 */
'use strict';
// const connection = require('../../database/connection');


let internals = {};
const settings = require('../../config/settings');
const axios = require('axios') ;


const getIdReport = async (pathField, arrayParameters,outputFormat) => {
    const {baseUrl}=settings;
      return axios({
          method:'post',
          url:baseUrl,
          data:{
            reportUnitUri: `${pathField}`,
            outputFormat,
            async: true,
            interactive: true,
            baseUrl: "/jrio",
            attachmentsPrefix: "/jrio/rest_v2/reportExecutions/{reportExecutionId}/exports/{contextPath}/attachments/",
            parameters: {
                reportParameter: arrayParameters
            }
        }
          
      }).then(response => {
          return response.data;
      }).catch(e=>{
          console.log('se encontró error')
          return e;
      })
    }

internals.createReportID = async function (request, h) {
    const {queryParameters, pathField,userEmail, userName,description,outputFormat} = request.payload;
    const db = request.mongo.db;
    const collectionJasperDescription = db.collection('jasper_report_description');
    // const arrayParameters=[];
    const resultRequest = await getIdReport(pathField,queryParameters,outputFormat);
    const parametersToQuery={}
    Object.values(queryParameters).map(res=>{
        parametersToQuery[res.name]=res.value[0];
    })
    const dataToInsertJasperDescription= { user:userName, email: userEmail, id_execute_report: resultRequest.requestId,outputFormat, show_report:false,
        link_report:"",
        description,parametersToQuery };
    //Insert into all reports Jasper
    collectionJasperDescription.insertOne(dataToInsertJasperDescription, function(err, res) {
        if (err) throw err;

        console.log("Se insertó correctamente");
      });

      return resultRequest;

}; 
internals.listReports =  function (request, h) {
    const {user} = request.params;
    const db = request.mongo.db;
    const collectionJasperDescription = db.collection('jasper_report_description');
   const queryPidm = {user:user};
 return  collectionJasperDescription.find(queryPidm).toArray()
 .then(result=>{
      return result!==null? result : []
  }).catch(e=>{
    throw e
});

 
};
module.exports = internals;